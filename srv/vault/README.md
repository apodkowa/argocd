## Vault configuration
1. init and unseal vault
```bash
kubectl -n base exec -it base-vault-0 -- vault operator init

 Unseal Key 1: tI9tjkBlFNLyfBPBKhf8xkPXkrkUXL4xALCR8YXhVo/1
 Unseal Key 2: u4IaEmnItX+C3fIqBGwE0zuBG3wxYy1LQFVwWkmBVVpr
 Unseal Key 3: RVXlBjh5FpnsnOPaJMZ7eO/4JjLiBs1VAF1AzEU4fOxA
 Unseal Key 4: 8GizlX2IrB9dSrTq/BxSzZrd4f+rKKXPMbJnjI5+pMRE
 Unseal Key 5: xnfmKLI0rhrZcr7o9iU20RvQGyPEQLhKaNysc7iApWn3

 Initial Root Token: s.SzGPJPqI2EynJuqGCZ34IRx6

 Vault initialized with 5 key shares and a key threshold of 3. Please securely
 distribute the key shares printed above. When the Vault is re-sealed,
 restarted, or stopped, you must supply at least 3 of these keys to unseal it
 before it can start servicing requests.

 Vault does not store the generated master key. Without at least 3 keys to
 reconstruct the master key, Vault will remain permanently sealed!

 It is possible to generate new unseal keys, provided you have a quorum of
 existing unseal keys shares. See "vault operator rekey" for more information.

#Use 3 of the 5 keys to unseal:
kubectl -n base exec -ti base-vault-0 -- vault operator unseal tI9tjkBlFNLyfBPBKhf8xkPXkrkUXL4xALCR8YXhVo/1
kubectl -n base exec -ti base-vault-0 -- vault operator unseal u4IaEmnItX+C3fIqBGwE0zuBG3wxYy1LQFVwWkmBVVpr
kubectl -n base exec -ti base-vault-0 -- vault operator unseal RVXlBjh5FpnsnOPaJMZ7eO/4JjLiBs1VAF1AzEU4fOxA
```
2. Create vault token

```bash
kubectl -n base exec -ti base-vault-0 -- sh
/$ vault login token=s.SzGPJPqI2EynJuqGCZ34IRx6
Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

/ $ vault token create
Key                  Value
---                  -----
token                s.Lc570TKmqdDlPY9wXVjkqy1x
```
3. Enable the kv backend

```bash
vault secrets enable -version=2 kv
```
4.  Create a test secret

```bash
vault kv put kv/test-secret foo=bar
```

